package biz.zwoop.executionbot;

import biz.zwoop.executionbot.model.Rule;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jimmy on 13/1/2017.
 */
public class RuleRunner {

    public void run(List<Rule> ruleList) {
        Map<String, Object> context = new HashMap<>();
        ruleList.forEach(
            rule -> RuleExecutorProvider.getRuleExecutor(rule.getType()).execute(context, rule.getParameters()));
    }
}
