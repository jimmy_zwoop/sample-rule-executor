package biz.zwoop.executionbot;

import biz.zwoop.executionbot.executor.*;
import biz.zwoop.executionbot.model.Rule;

/**
 * Created by jimmy on 13/1/2017.
 */
public final class RuleExecutorProvider {

    private RuleExecutorProvider() {}

    public static RuleExecutor getRuleExecutor(Rule.Type ruleType) {
        // new instance is not good, may be cached or managed by spring
        // annotation may help for rule - executor linkage
        switch (ruleType) {
            case GET: return new GetRuleExecutor();
            case CLICK: return new ClickRuleExecutor();
            case INPUT: return new InputRuleExecutor();
            case WAIT: return new WaitRuleExecutor();
            default: return null;
        }
    }
}
