package biz.zwoop.executionbot.model;

import java.util.Map;

/**
 * Created by jimmy on 13/1/2017.
 */
public class Rule {

    public enum Type {
        WAIT, GET, CLICK, INPUT
    }

    private Type type;
    private Map<String, Object> parameters;

    public Rule(Type type, Map<String, Object> parameters) {
        this.type = type;
        this.parameters = parameters;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }
}
