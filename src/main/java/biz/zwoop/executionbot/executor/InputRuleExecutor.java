package biz.zwoop.executionbot.executor;

import java.util.Map;

/**
 * Created by jimmy on 13/1/2017.
 */
public class InputRuleExecutor implements RuleExecutor {

    public void execute(Map<String, Object> context, Map<String, Object> parameters) {
        String value = (String) context.get(parameters.get("value")); // expr like ${xxx} may use to separate static and dynamic content
        String selector = (String) parameters.get("selector");
        System.out.println("filling in " + selector + " with value " + value);
    }
}
