package biz.zwoop.executionbot.executor;

import java.util.Map;

/**
 * Created by jimmy on 13/1/2017.
 */
public class ClickRuleExecutor implements RuleExecutor {

    public void execute(Map<String, Object> context, Map<String, Object> parameters) {
        String selector = (String) parameters.get("selector");
        System.out.println("Click on " + selector);
    }

}
