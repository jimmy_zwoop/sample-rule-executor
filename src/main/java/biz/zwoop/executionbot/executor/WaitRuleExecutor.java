package biz.zwoop.executionbot.executor;

import java.util.Map;

/**
 * Created by jimmy on 13/1/2017.
 */
public class WaitRuleExecutor implements RuleExecutor {

    public void execute(Map<String, Object> context, Map<String, Object> parameters) {
        // just wait
        System.out.println("I am waiting");
    }
}
