package biz.zwoop.executionbot;

import biz.zwoop.executionbot.model.Rule;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jimmy on 13/1/2017.
 */
public class Main {

    public static void main(String[] args) {
        // prepare rule
        Map<String, Object> param1 = new HashMap<>();
        param1.put("selector", "span.desc");
        param1.put("contextName", "description");
        Rule rule1 = new Rule(Rule.Type.GET, param1);

        Map<String, Object> param2 = new HashMap<>();
        param2.put("value", "description");
        param2.put("selector", "input.desc");
        Rule rule2 = new Rule(Rule.Type.INPUT, param2);

        Map<String, Object> param3 = new HashMap<>();
        param3.put("selector", "button.add");
        Rule rule3 = new Rule(Rule.Type.CLICK, param3);

        Map<String, Object> param4 = new HashMap<>();
        Rule rule4 = new Rule(Rule.Type.WAIT, param4);

        List<Rule> ruleList = Arrays.asList(rule1, rule2, rule3, rule4);

        // execute the rules
        new RuleRunner().run(ruleList);
    }
}
